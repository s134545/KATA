import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class AddSteps {

    Calculator calculator = new Calculator();
    int result;

    @Given("No arguments")
    public void no_arguments_are_passed_to_the_Add_function() throws Exception {

    }

    @When("Add is called")
    public void add_is_called() throws Exception {
        result = calculator.Add();
    }

    @Then("The Add function returns {int}")
    public void the_Add_function_returns(Integer int1) throws Exception {
        assertEquals(0, result);
    }

    @Given("{int} and {int} is passed to the Add function")
    public void and_is_passed_to_the_Add_function(Integer int1, Integer int2) throws Exception {
        result = calculator.Add("1", "2");
    }

    @Then("{int} is returned")
    public void is_return(Integer int1) throws Exception {
        assertEquals(3, result);
    }

}
