import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddTest {

    @Test
    public void it_can_have_no_arguments() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add(), 0);
    }

    @Test
    public void it_is_capable_of_taking_only_one_argument() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add("2"), 2);
    }

    @Test
    public void it_is_capable_of_taking_two_arguments() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add("2", "3"), 5);
    }

    @Test
    public void it_is_capable_of_taking_unknown_amount_of_numbers() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add("1", "2", "3", "4", "5", "6"), 21);
    }

    @Test
    public void it_is_capable_of_taking_newlines_instead_of_commas_between_numbers() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add("1\n2", "3"), 6);
    }

    @Test
    public void it_is_possible_to_set_custom_delimiter() throws Exception {
        Calculator calculator = new Calculator();

        assertEquals(calculator.Add("//;\n1;3;5"), 9);
    }

    @Test
    public void it_throws_an_exception_when_a_negative_number_is_given() throws Exception {
        Calculator calculator = new Calculator();

        try {
            calculator.Add("1", "-2", "3");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "negatives not allowed: -2");
        }
    }

    @Test
    public void it_throws_an_exception_when_negative_numbers_are_given() throws Exception {
        Calculator calculator = new Calculator();

        try {
            calculator.Add("1", "-2", "-3");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "negatives not allowed: -2, -3");
        }
    }
}
