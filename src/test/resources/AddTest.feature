Feature: It can add numbers
  Our calculator can add numbers in different ways.
  Scenario: No arguments are given
    Given No arguments
    When Add is called
    Then The Add function returns 0

  Scenario: Two positive numbers are given
    Given 1 and 2 is passed to the Add function
    Then 3 is returned