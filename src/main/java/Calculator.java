import java.util.ArrayList;
import java.util.List;

public class Calculator {

    public int Add(String... numbers) throws Exception {
        int sum = 0;
        List<String> negatives = new ArrayList<String>();

        if (numbers.length == 0) {
            return 0;
        }

        if (numbers[0].contains("//")) {
            String delimeter = String.valueOf(numbers[0].charAt(2));
            numbers = numbers[0].substring(4).split(delimeter);
        }

        for (String number : numbers) {
            for (String num : number.split("\n")) {
                int parsedNum = Integer.parseInt(num);
                if (parsedNum < 0) {
                    negatives.add(num);
                } else {
                    sum += parsedNum;
                }
            }
        }

        if (negatives.size() > 0) {
            String message = "negatives not allowed: ";
            String negNumbers = String.join(", ", negatives);
            message+=negNumbers;

            throw new Exception(message);
        }

        return sum;
    }
}
